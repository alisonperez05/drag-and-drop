import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import { HomeView, DashboardView } from "./views";

const AppRouter = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" >
          <HomeView />
        </Route>
        <Route path="/dashboard" >
          <DashboardView />
        </Route>
      </Switch>
    </Router>
  )
}

export default AppRouter;