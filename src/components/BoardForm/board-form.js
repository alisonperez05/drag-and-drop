import React, { useState } from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { Button, Form, FormControl, InputGroup } from 'react-bootstrap';
import { MdAdd } from 'react-icons/md';
import * as boardsService from '../../services/boards-service';
import FxOverlay from '../FxOverlay';
import './board-form.css';

const BoardForm = () => {
  const [name, setName] = useState('');
  const [show, setShow] = useState(false);

  const stages = useStoreState(state => state.stages.items);
  const setSelectedBoard = useStoreActions(actions => actions.boards.setSelectedBoard);

  const handleChange = (e) => {
    setName(e.target.value);
  }

  const onCreateBtnClick = () => {
    setShow(!show);

    if (name.length > 3) {

      const board = {
        name,
        created_at: Date.now()
      }

      boardsService.createBoard(board).then(doc => {
        setSelectedBoard(({id: doc.id, ...board}));
        boardsService.createBoardStagesRecord(doc.id, stages).then(docRef => {
        }).catch(err => {
          console.log('Error in batch: ', err)
        });
      }).catch(err => console.log('ERROR: ', err));
    }

  }

  const boardFormContent = (
      <Form className="create-boardForm">
        <InputGroup>
          <FormControl autoFocus size="sm" value={name} onChange={handleChange} />
          <InputGroup.Append>
            <Button variant="primary" size="sm" onClick={onCreateBtnClick} >Create</Button>
          </InputGroup.Append>
        </InputGroup>
      </Form>
    )

  return (
    <div className="nav-item">
      <FxOverlay show={show} contentEl={boardFormContent} title="Board Form" className="overlay-create-board" >
        <Button onClick={() => setShow(!show)} variant="light" >
          <MdAdd style={{ fontSize: 18 }} />
          <span>Create New</span>
        </Button>
      </FxOverlay>
    </div>
  )
}

export default BoardForm;