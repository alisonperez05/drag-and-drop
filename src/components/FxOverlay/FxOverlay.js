import React from 'react';
import { Popover, OverlayTrigger } from "react-bootstrap";

const FxOverlay = ({ show, trigger = 'click', title, titleAs = 'h3', contentEl, placement = 'bottom', className = '', children }) => {
    const popover = (
      <Popover className={className}>
        {title && (
          <Popover.Title as={titleAs}>{title}</Popover.Title>
        )}
        <Popover.Content>
          {contentEl}
        </Popover.Content>
      </Popover>
    );
    return (
      <OverlayTrigger trigger={trigger} placement={placement} overlay={popover} show={show} >
        {children}
      </OverlayTrigger>
    );
  }

  export default FxOverlay;