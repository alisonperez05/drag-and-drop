import { thunk, action } from 'easy-peasy';
import * as service from '../services/boards-service';
import * as usersService from '../services/users-service';

const model = {
    isLoaded: false,
    items: [],
    users: [],
    selectedBoard: { id: '', name: 'Select a board' },
    setSelectedBoard: action((state, payload) => {
        state.selectedBoard = payload;
    }),
    setIsLoaded: action((state, payload) => {
        state.isLoaded = payload;
    }),
    addBoard: action((state, payload) => {
        state.items.push(payload);
    }),
    removeBoard: action((state, payload) => {
        state.items = state.items.filter(board => board.id != payload);
    }),
    loadUsers: thunk(async (actions, payload) => {
        const resp = await usersService.fetchUsers();
        const data = resp.docs.map(doc => ({id: doc.id, ...doc.data()}));
        actions.setUsers(data);
    }),
    setUsers: action((state, payload) => {
        state.users = payload;
    }),
    loadResources: thunk(async (actions, payload) => {
        service.suscribeToBoards({ onAdded: actions.addBoard, onRemoved: actions.removeBoard });
        actions.loadUsers();
        actions.setIsLoaded(true);
    })
}

export default model;