import { action, thunk } from "easy-peasy";
import * as service from "../services/stages-service";

const model = {
    items: [],
    boardStages: [],
    tags: ['Blog post', 'Long form', 'SEO article'],
    tagFilters: [],
    searchFilter: '',
    loadStages: thunk(async (actions, payload) => {
        const resp = await service.fetchStages();
        const data = resp.docs.map(doc => doc.data());
        actions.setStages(data);
    }),
    setStages: action((state, payload) => {
        state.items = payload;
    }),
    onAdded: action((state, payload) => {
        state.boardStages.push(payload);
    }),
    onModified: action((state, payload) => {
        state.boardStages[payload.index - 1] = payload;
    }),
    clearBoardStages: action((state, payload) => {
        state.boardStages = [];
    }),
    setTagFilters: action((state, payload) => {
        state.tagFilters = payload;
    }),
    setSearchFilter: action((state, payload) => {
        state.searchFilter = payload;
    })
}

export default model;