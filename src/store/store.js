import { createStore } from 'easy-peasy';
import boardsStoreModel from './boards-store-model';
import stagesStoreModel from './stages-store-model';

const storeModel = {
    boards: boardsStoreModel,
    stages: stagesStoreModel
};

const store = createStore(storeModel);

export default store;