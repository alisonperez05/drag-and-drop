import React, { useEffect } from "react";
import { useStoreState, useStoreActions } from "easy-peasy";
import { Navbar, Form, Dropdown, DropdownButton } from "react-bootstrap";
import { MdSupervisorAccount, MdBugReport, MdDashboard, MdAccountCircle } from "react-icons/md";
import { BoardForm } from "../../../../components";
import "./topbar.css";

const Topbar = () => {

  const boards = useStoreState(state => state.boards.items);
  const selectedBoard = useStoreState(state => state.boards.selectedBoard);
  const setSelectedBoard = useStoreActions(actions => actions.boards.setSelectedBoard);

  useEffect(() => {
  }, [boards])

  const onMenuIconClick = () => {
    const sidenav = document.getElementById("sidenav");
    if (sidenav.style.display != 'block') {
      sidenav.style.display = 'block';
    } else {
      sidenav.style.display = 'none';
    }
  }

  const onBoardSelect = (e) => {
    const id = e.target.value;
    const selected = boards.filter(board => board.id == id)[0];
    setSelectedBoard(selected || { id: '' });
  }


  return (
    <div>
      <Navbar variant="dark" className="navbar main">
        <div className="brand" >
          <label>MarketingCloud FX</label>
        </div>
        <div className="nav-items-containter">
          <div className="nav-items-left">
            <div className="nav-item">
              <Form.Control value={selectedBoard.id} onChange={onBoardSelect} as="select" size="sm">
                <option value={''} >Select a Board</option>
                {boards.map(board => <option value={board.id} key={board.id} >{board.name}</option>)}
              </Form.Control>
            </div>
            <div className="nav-item" style={{ marginLeft: 2 }}>
              <BoardForm />
            </div>
          </div>
        </div>
        <div className="nav-items-right topbar-icons">
          <div className="nav-item">
            <MdDashboard />
          </div>
          <div className="nav-item">
            <MdSupervisorAccount />
          </div>
          <div className="nav-item">
            <MdBugReport />
          </div>
          <div className="nav-item account-section">
            <MdAccountCircle />
            <DropdownButton variant="light" title="Account">
              <Dropdown.Item href="#">Logout</Dropdown.Item>
            </DropdownButton>
          </div>
        </div>
        <div className="nav-item menu_icon_container" >
          <a onClick={onMenuIconClick} >
            <span className="material-icons">menu</span>
          </a>
        </div>
      </Navbar>
    </div>
  );
};

export default Topbar;
