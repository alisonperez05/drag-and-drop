import React from "react";
import { Row, Col } from "react-bootstrap";
import { Link } from 'react-router-dom';
import { MdFilterNone } from "react-icons/md";
import "./sidenav.css";

const Sidenav = () => {
  return (
    <div id="sidenav" className="sidenav">
      <div className="menu-item-section">
        <MdFilterNone /> Content
      </div>
      <Link to="/dashboard" className="sidenav-item">
        Dashboard
        </Link>
      <Link to="/" className="sidenav-item">
        Home Page
        </Link>
    </div>
  );
};

export default Sidenav;
