import React from "react";
import { Sidenav, Topbar } from "./components";
import "./main-layout.css";

const MainLayout = ({ children }) => {
  return (
    <div>
      <Topbar />
      <div className="main-container">
        <Sidenav />
        <div className="main-content">
          {children}
        </div>
      </div>
    </div>
  );
};

export default MainLayout;
