import firebase from './firebase-service';
import { USERS } from './contants';

const db = firebase.firestore();

const fetchUsers = async () => {
    return await db.collection(USERS).orderBy('name').get();
}

export { fetchUsers }