import firebase from './firebase-service';
import { BOARDS_STAGES, STAGES } from './contants';

const db = firebase.firestore();

const fetchStages = async () => {
    return await db.collection(STAGES).orderBy('index').get();
}

const updateStage = async (boardId, id, payload) => {
    return await db.collection(BOARDS_STAGES).doc(boardId)
    .collection(STAGES).doc(id).update(payload);
}

const subscribeToBoardStages = (boardId, {onAdded, onRemoved, onModified}) => {
    return db.collection(BOARDS_STAGES).doc(boardId)
    .collection(STAGES).orderBy('index').onSnapshot(snapshot => {
        snapshot.docChanges().forEach(e => {
            if(e.type == 'added') {
                onAdded({id: e.doc.id, ...e.doc.data()});
            }
            if(e.type == 'modified') {
                onModified({id: e.doc.id, ...e.doc.data()});
            }
        })
    })
}

const getStagesByBoardId = async (boardId) => {
    return await db.collection(BOARDS_STAGES).doc(boardId).collection(STAGES).orderBy('index').get();
}

export { getStagesByBoardId, fetchStages, updateStage, subscribeToBoardStages }