const BOARDS = 'boards';
const BOARDS_STAGES = 'boards-stages';
const STAGES = 'stages';
const USERS = 'users';

export { BOARDS, BOARDS_STAGES, STAGES, USERS };