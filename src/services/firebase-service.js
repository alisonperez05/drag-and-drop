import firebase from 'firebase/app';
import 'firebase/analytics';
import 'firebase/database';
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyDw_NAE-cBez7bD7Cu2iVPrTFmFjxW1KiI",
    authDomain: "workflow-fx.firebaseapp.com",
    databaseURL: "https://workflow-fx.firebaseio.com",
    projectId: "workflow-fx",
    storageBucket: "workflow-fx.appspot.com",
    messagingSenderId: "802942389526",
    appId: "1:802942389526:web:4d08fdb9fa3730f72b9437",
    measurementId: "G-KSD1HMP27S"
};

const fire = firebase.initializeApp(firebaseConfig);

export default fire;
