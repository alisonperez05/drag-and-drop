import firebase from './firebase-service';
import { BOARDS, BOARDS_STAGES, STAGES } from './contants';

const db = firebase.firestore();

const suscribeToBoards = ({ onAdded, onRemoved }) => {
    db.collection(BOARDS).onSnapshot(snapShot => {
        snapShot.docChanges().forEach(e => {
            if (e.type == 'added') {
                onAdded({ id: e.doc.id, ...e.doc.data() });
            } else if (e.type == 'removed') {
                onRemoved(e.doc.id);
            }
        });
    });
}

const createBoard = async (board) => {
    return await db.collection(BOARDS).add(board);
}

const createBoardStagesRecord = async (id, stages) => {
    const batch = db.batch(); // opening a batch process in order to commit all at once
    const ref = db.collection(BOARDS_STAGES).doc(id).collection(STAGES);
    stages.forEach(stage => {
        batch.set(ref.doc(), stage);
    })
    return await batch.commit();
};

const updateBoard = async (id, board) => {
    return await db.collection(BOARDS).doc(id).update(board);
}

export { createBoard, createBoardStagesRecord, updateBoard, suscribeToBoards }