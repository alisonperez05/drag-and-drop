import React from 'react';
import './app.css';
import AppRouter from './routes';
import { StoreProvider } from 'easy-peasy';
import store from './store';

const App = (props) => {

    return (
        <StoreProvider store={store}>
            <AppRouter />
        </StoreProvider>
    );
}

export default App;