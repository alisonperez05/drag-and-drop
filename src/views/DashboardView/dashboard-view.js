import React, { useEffect } from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { Container } from "react-smooth-dnd";
import { MainLayout } from '../../layouts';
import * as stagesService from '../../services/stages-service';
import { FxStage, DashboardTopbar } from './components';
import './dashboard.css';

const DashboardView = () => {

    // Board setup
    const loadResources = useStoreActions(actions => actions.boards.loadResources);
    const isLoaded = useStoreState(state => state.boards.isLoaded);
    const selectedBoard = useStoreState(state => state.boards.selectedBoard);
    // Stages setup
    const clearBoardStages = useStoreActions(actions => actions.stages.clearBoardStages);
    const onAdded = useStoreActions(actions => actions.stages.onAdded);
    const onModified = useStoreActions(actions => actions.stages.onModified);;
    const loadStages = useStoreActions(actions => actions.stages.loadStages);
    const stages = useStoreState(state => state.stages.boardStages);

    useEffect(() => {
        if (!isLoaded) {
            console.log('loading resources...');
            loadResources();
            loadStages();
        }
    }, []);

    useEffect(() => {
        if (selectedBoard.id) {
            clearBoardStages();
            const unsusbcribe = stagesService.subscribeToBoardStages(selectedBoard.id, { onAdded: onAdded, onModified: onModified });
            return () => unsusbcribe;
        }
    }, [selectedBoard]);

    const onColumnDrop = () => { }

    const renderBoard = () => {
        if (selectedBoard.id != '') {
            return (
                <>
                    <DashboardTopbar />
                    <Container
                        className="stages-container"
                        orientation="horizontal"
                        onDrop={onColumnDrop}
                        dragHandleSelector=".stage-holder"
                        dropPlaceholder={{
                            animationDuration: 150,
                            showOnTop: true,
                            className: 'cards-drop-preview'
                        }} >
                        {
                            stages.map(stage =>
                                <div className="stage-holder" key={`stage-${stage.name}`}>
                                    <FxStage stage={stage} />
                                </div>
                            )
                        }
                    </Container>
                </>
            )
        }

        return (
            <>
                <div style={{ textAlign: 'center', marginTop: 70 }} >
                    <h2>What's next?</h2>
                    <p>Select or Create a <strong>Board</strong> and <strong>Enjoy!</strong> </p>
                </div>
            </>
        )
    }

    return (
        <MainLayout>
            <div className="dashboard">
                {renderBoard()}
            </div>
        </MainLayout>
    );
}

export default DashboardView;