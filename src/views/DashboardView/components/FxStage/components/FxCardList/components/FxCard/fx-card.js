import React from 'react';
import { Card, Form } from 'react-bootstrap';
import { MdEvent, MdWidgets } from 'react-icons/md';
import moment from 'moment';
import './fx-card.css'

const FxCard = ({ card, selected, onCardClick =()=>{}, handleCardSelect =()=>{}, styles = {}, styleClasses = '' }) => {

    const onSelect = (e) => {
        e.stopPropagation();
        handleCardSelect();
    }

    return (
        <Card className={`fx-card ${selected ? 'selected-card' : '' } ${styleClasses} `} onClick={onCardClick} style={styles} >
            <Card.Body>
                <Card.Title className="fx-card-title" >
                    {card.title} 
                    <Form.Check onClick={onSelect} onChange={()=>{}} checked={selected}  />
                    </Card.Title>
                <Card.Text className="card-description">
                    {card.description}
                </Card.Text>
                <div className="fx-tag">
                    <MdWidgets />
                    <span>{card.tag}</span>
                </div>
                <div className="fx-card-footer">
                    {card.dueDate && (
                        <>
                            <hr className="divider" />
                            <div className="fx-tag">
                                <MdEvent />
                                <span>{moment(card.dueDate).format('L')}</span>
                            </div>
                        </>
                    )}
                </div>
            </Card.Body>
        </Card>
    );
}

export default FxCard;