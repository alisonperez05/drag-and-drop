import React from 'react';
import { useStoreState } from 'easy-peasy';
import { Draggable } from 'react-smooth-dnd';
import { FxCard } from './components';

const FxCardList = ({ cards, onCardClick, cardsToMove, handleCardSelect }) => {

    const tagFilters = useStoreState(state => state.stages.tagFilters);
    const searchFilter = useStoreState(state => state.stages.searchFilter);

    if (!cards) {
        return <></>;
    }

    const isCardSelected = (index) => {
        return cardsToMove.includes(index);
    }

    const shouldRenderCard = (card) => {
        if (tagFilters.length > 0 && !tagFilters.includes(card.tag)) {
            return false;
        }
        if (searchFilter.length > 1 && !card.title.toLowerCase().includes(searchFilter.toLowerCase())) {
            return false;
        }
        return true;
    }

    const renderSelectedCards = () => {

        if (!cardsToMove.length) {
            return <></>;
        }
        
        const res =  cardsToMove.map(index => ({ index, card: cards[index] }));
        
        return (
            <Draggable className="cards-to-move" style={{ marginTop: 5, cursor: 'pointer' }} >
                {res.map((obj, i) => {

                    const { card, index } = obj;
                    
                    if(!shouldRenderCard(card)) {
                        return <span key={`card-selected${index}`}></span>
                    }

                    const styles = i > 0 ? {
                        marginTop: -110,
                        width: '100%'
                    } : {} ;
                    return (
                        <FxCard styles={styles}  key={`card-selected${index}`} card={card} selected={true} handleCardSelect={() => handleCardSelect(index)} />
                    )
                })}
            </Draggable>
        )
    }

    const renderNoSelectedCards = () => {

        const res = cards.map((card, index) => ({ index, card })).filter((obj, index) => !isCardSelected(index));

        if (!res.length) {
            return <></>
        }

        return (
            <>
                {res.map(obj => {
                    const { card, index } = obj;

                    if(!shouldRenderCard(card)) {
                        return <span key={`card-no-selected${index}`}></span>
                    }

                    if (cardsToMove.length) { // disabling drag and drop for unselected cards
                        return (
                            <FxCard styles={{ marginTop: 5 }} card={card} selected={false} handleCardSelect={() => handleCardSelect(index)} key={`card-no-selected${index}`} />
                        )
                    }

                    return (
                        <Draggable className="cards-no-selected" style={{ marginTop: 5, cursor: 'pointer' }} key={`card-no-selected${index}`} >
                            <FxCard card={card} selected={false} onCardClick={() => onCardClick(card, index)} handleCardSelect={() => handleCardSelect(index)} />
                        </Draggable>
                    )
                })}
            </>
        )
    }

    return (
        <>
            {renderSelectedCards()}
            {renderNoSelectedCards()}
        </>
    )
}

export default FxCardList;