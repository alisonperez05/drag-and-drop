import React, { useState } from 'react';
import { Modal, Form, Button, Row, Col } from 'react-bootstrap';
import ReactDatePicker from 'react-datepicker';

import "react-datepicker/dist/react-datepicker.css";
import { useStoreState } from 'easy-peasy';


const FxCardForm = ({ show, handleClose, handleSave, selectedCard, setSelectedCard }) => {

    const tags = useStoreState(state => state.stages.tags);
    const users = useStoreState(state => state.boards.users);

    const handleChange = ({ target }) => {
        setSelectedCard(oldValues => ({ ...oldValues, [target.name]: target.value }));
    }

    return (
        <Modal centered show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>{selectedCard.created_at > '' ? 'Edit ' : 'Add '} Card</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <Form.Group>
                        <Form.Label>Title</Form.Label>
                        <Form.Control required size="sm" onChange={handleChange} name="title" value={selectedCard.title} type="text" placeholder="Enter title" />
                    </Form.Group>
                    <Row>
                        <Col>
                            <Form.Group>
                                <Form.Label>Tag</Form.Label>
                                <Form.Control size="sm" onChange={handleChange} name="tag" value={selectedCard.tag} size="sm" as="select">
                                    <option>--Select--</option>
                                    {
                                        tags.map(tag => <option key={tag} >{tag}</option>)
                                    }
                                </Form.Control>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group>
                                <Form.Label>Assignee</Form.Label>
                                <Form.Control size="sm" onChange={handleChange} name="assignee" value={selectedCard.assignee} size="sm" as="select">
                                    <option>--Select--</option>
                                    {
                                        users.map(user => <option key={user.id} >{user.name}</option>)
                                    }
                                </Form.Control>
                            </Form.Group>
                        </Col>
                    </Row>
                    <div>
                        <Row>
                            <Col>
                                <label>Due Date</label>
                            </Col>
                        </Row>
                        <Row>
                            <Col>
                                <ReactDatePicker
                                    style={{ marginBottom: 10 }}
                                    className="form-control form-control-sm"
                                    selected={selectedCard.dueDate}
                                    onChange={date => setSelectedCard(oldValues => ({ ...oldValues, dueDate: date ? date.getTime() : '' }))}
                                />
                            </Col>
                        </Row>
                    </div>
                    <Form.Group>
                        <Form.Label>Description</Form.Label>
                        <Form.Control onChange={handleChange} name="description" value={selectedCard.description} as="textarea" rows="3" placeholder="Enter description" />
                    </Form.Group>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                 </Button>
                <Button variant="primary" onClick={handleSave}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
    )
}

export default FxCardForm;