import { useStoreState } from 'easy-peasy';
import React, { useEffect, useState } from 'react';
import { Badge } from 'react-bootstrap';
import { MdAdd, MdDoneAll, MdMoreHoriz, MdStar } from 'react-icons/md';
import { Container } from 'react-smooth-dnd';
import { FxOverlay } from '../../../../components';
import * as stagesService from '../../../../services/stages-service';
import { FxCardForm, FxCardList } from './components';
import './fx-stage.css';

const INITIAL_CARD_VALUES = { title: '', description: '', created_at: '', tag: '', dueDate: null, assignee: '' };

const FxStage = ({ stage, styles, classes }) => {

    const [showCardForm, setShowCardForm] = useState(false);
    const [showStageMenu, setShowStageMenu] = useState(false);
    const selectedBoard = useStoreState(state => state.boards.selectedBoard);
    const [selectedCard, setSelectedCard] = useState(INITIAL_CARD_VALUES);
    const [selectedCardIndex, setSelectedCardIndex] = useState(0);
    const [cardsToMove, setCardsToMove] = useState([]);

    const requireCardForm = stage.name == 'Requested';
    const showAppoveStageMenu = stage.name == 'Pending Approval';

    useEffect(() => {
        if (stage.cards == undefined) {
            stage.cards = [];
        }
    }, []);

    const handleCardFormOpen = () => {
        setShowCardForm(true);
    }

    const handleCardFormClose = () => {
        setShowCardForm(false);
    }

    const handleCardSave = () => {

        const isNew = !selectedCard.created_at > '';
        const boardId = selectedBoard.id;
        const { id, cards } = stage;

        if (isNew) {
            selectedCard.created_at = Date.now();
            cards.push(selectedCard);
        } else {
            cards[selectedCardIndex] = selectedCard;
        }

        handleCardFormClose();

        stagesService.updateStage(boardId, id, { cards })
            .then(resP => {
            }).catch(err => console.log('Card error:', err))
            .finally(() => setSelectedCard(INITIAL_CARD_VALUES));
    }

    const onCardClick = (card, index) => {
        setSelectedCardIndex(index);
        setSelectedCard(card);
        handleCardFormOpen();
    }

    const initCardValues = () => {
        setSelectedCard(INITIAL_CARD_VALUES);
    }

    const onNewCardClick = () => {
        initCardValues();
        setTimeout(() => {
            handleCardFormOpen();
        }, 50)
        setShowStageMenu(false);
    }

    const toggleActionsMenu = () => {
        setShowStageMenu(!showStageMenu);
    }

    const handleCardSelect = (index) => {
        if (cardsToMove.includes(index)) {
            setCardsToMove(cardsToMove.filter(i => i != index));
        }
        else {
            setCardsToMove(cardsToMove.concat(index));
        }
    }

    const getCardsToDrop = (index) => {
        if(cardsToMove.length){
            return cardsToMove.map(i => stage.cards[i]);
        }
        return [].concat(stage.cards[index]);
    }

    const onDropEnd = (dropResult) => {
        const { removedIndex, addedIndex, payload } = dropResult;

        if (removedIndex === null && addedIndex === null) {
            return stage.cards;
        }

        let result = [...stage.cards];

        if (removedIndex !== null) {
            if(cardsToMove.length) {
                result = stage.cards.filter((card, index) => !cardsToMove.includes(index));
            } else {
                result.splice(removedIndex, 1);
            }
            setCardsToMove([]);
        }

        if (addedIndex !== null) {
            payload.forEach((cardToAdd, i) => {
                result.splice(addedIndex + i, 0, cardToAdd);
            })
        }

        const boardId = selectedBoard.id;
        stagesService.updateStage(boardId, stage.id, { cards: result });
    }

    const stageActionsMenu = (
        <div>
            <div className="overlay-menu-item text-muted" >{cardsToMove.length} Items Selected</div>
            {showAppoveStageMenu && (
                <>
                    <hr className="divider" />
                    <div className="overlay-menu-item text-muted" ><MdDoneAll />{' '}<span>Approve</span></div>
                </>
            )}
            <hr className="divider" />
            <div className="overlay-menu-item text-muted" ><MdStar />{' '}<span>Rate</span></div>
            {
                requireCardForm && (
                    <>
                        <hr className="divider" />
                        <div className="overlay-menu-item actions" onClick={onNewCardClick} >
                            <MdAdd /><span>Add Card</span>
                        </div>
                    </>
                )
            }
        </div>
    )

    return (
        <div className={`stage ${classes}`} style={styles} >
            <div className="stage-header">
                <div className="stage-header-left" >
                    <Badge style={{ backgroundColor: `${stage.cardsCountColor}` }} >{stage.cards ? stage.cards.length : 0}</Badge>
                    <h6>{stage.name}</h6>
                </div>
                <FxOverlay show={showStageMenu} contentEl={stageActionsMenu} className="overlay-show-stage-menu" >
                    <MdMoreHoriz onClick={toggleActionsMenu} className="stage-actions-menu" />
                </FxOverlay>
            </div>

            <div className="cards-holder">
                <Container
                    type="vertical"
                    groupName="col"
                    dropPlaceholderAnimationDuration={200}
                    getChildPayload={getCardsToDrop}
                    onDrop={onDropEnd}
                    dropPlaceholder={{
                        animationDuration: 150,
                        showOnTop: true,
                        className: 'drop-preview'
                    }}
                >
                    <FxCardList stage={stage} cards={stage.cards} onCardClick={onCardClick} cardsToMove={cardsToMove} handleCardSelect={handleCardSelect} />
                </Container>
            </div>

            <FxCardForm selectedCard={selectedCard} setSelectedCard={setSelectedCard} show={showCardForm} handleClose={handleCardFormClose} handleSave={handleCardSave} />

        </div>
    );

}

export default FxStage;