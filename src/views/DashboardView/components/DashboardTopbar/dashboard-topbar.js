import React from 'react';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Navbar, Form, FormControl, Button, } from "react-bootstrap";
import { MdSort, MdFilterList, MdSearch, MdRemoveRedEye } from "react-icons/md";
import { FxOverlay } from '../../../../components';

const DashboardTopbar = () => {

    const searchFilter = useStoreState(state => state.stages.searchFilter);
    const setSearchFilter = useStoreActions(actions => actions.stages.setSearchFilter);
    const tags = useStoreState(state => state.stages.tags);
    const selectedTags = useStoreState(state => state.stages.tagFilters);
    const setSelectedTags = useStoreActions(actions => actions.stages.setTagFilters);

    const handleFilterChange = (e, tag) => {

        const { checked } = e.target;
        let res = [...selectedTags];

        if (checked) {
            res.push(tag);
        } else {
            res = res.filter(t => t != tag);
        }

        setSelectedTags(res);
    }

    const filtersContent = (
        <Form>
            {tags.map(tag => <Form.Check checked={selectedTags.includes(tag)} onChange={(e) => handleFilterChange(e, tag)} className="fx-tag-checkbox" key={tag} type='checkbox' label={tag} />)}
        </Form>
    )

    return (
        <Navbar variant="dark" className="navbar secundary">
            <div className="nav-items-containter" style={{ marginLeft: 0 }}>
                <div className="nav-items-left">
                    <MdRemoveRedEye style={{ color: '#BDBDBD', fontSize: 26 }} />
                    <label style={{ marginLeft: 8 }} >Statuses</label>
                </div>
                <div className="nav-items-right">
                    <Form className="nav-item">
                        <FxOverlay contentEl={filtersContent} className="fx-popover" >
                            <Button variant="light">
                                <MdFilterList /><span>Filter</span>
                            </Button>
                        </FxOverlay>
                    </Form>
                    <Form className="nav-item">
                        <Button variant="light" >
                            <MdSort /><span>Sort</span>
                        </Button>
                    </Form>
                    <Form className="nav-item search-form">
                        <MdSearch />
                        <FormControl onChange={e => setSearchFilter(e.target.value)} value={searchFilter} type="text" placeholder="Search" />
                    </Form>
                </div>
            </div>
        </Navbar>
    )
}

export default DashboardTopbar