import React from 'react';
import { MainLayout } from '../../layouts';

const HomeView = () => {
    return (
        <MainLayout>
            <div style={{textAlign: 'center', marginTop: 70}} >
                <h2>Welcome to the best test project</h2>
                <p>Use the the navigation on the left and click over Dashboard</p>
            </div>
        </MainLayout>
    )
}

export default HomeView